#!/bin/bash

SAVEDATE=`date +%Y%m%d-%H%M%S-%s`

if [ -e ~/.vimrc ]
then
    mv ~/.vimrc ~/.vimrc_$SAVEDATE
fi

ln -sf $PWD/vim/vimrc ~/.vimrc




